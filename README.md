# RandomCorpus

Micro CLI pour générer un sous-corpus de `n` lignes choisies au hasard.
(`n` est fourni par l'utilisateur.ice, valeur défaut de 100).

Processus:
- lier les entrées du tableur iconographie avec des fichiers images
- sélectionner `n` lignes du tableur
- produire un corpus de sortie (version réduite du tableur + dossier
  contenant toutes les images).
- zipper le tout dans `./randomcorpus_{n}.zip`

---

## Utilisation:

Arguments (optionnels): 
* `-n` `--numberofrows`: le nombre de lignes à conserver en sortie
* `-d` `--directrepr`: ne conserver que des représentations directes du quartier

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python main.py -n <nombre de lignes en sortie>? -d?
```

---

## Structure attendue

```
root/
  |__in/                                      (dossier d'entrée)
  |  |__iconography_viviennebourse_240304.csv (ou autre fichier d'entrée)
  |  |__images/                               (dossier contenant les images)
  |__main.py                                  (script)
  |__out/                                     (dossier de sortie)
     |__iconography_viviennebourse_240304.csv (ou autre, selon le nom du fichier utilisé en entrée)
     |__images/                               (images produites en sortie)
```

---

## Licence

GNU GPL 3.0
