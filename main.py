import pandas as pd
import typing as t
import argparse
import shutil
import os
import re


# *********************************************************
# functions and constants
# *********************************************************
ICONO_HEADERS = [
  "institution"
  , "index"
  , "url_manifest"
  , "url_source"
  , "title"
  , "title_secondary"
  , "author_1"
  , "author_2"
  , "publisher"
  , "date_source"
  , "date_corr"
  , "technique"
  , "inventory_number"
  , "corpus"
  , "description"
  , "inscription"
  , "commentary"
  , "theme"
  , "named_entity"
  , "id_location"
  , "internal_note"
  , "produced_richelieu"
  , "represents_richelieu"
]


def idx_normalise(idx_match:re.Match) -> str:
    """
    normalise the a simple index based on the index's
    match object. only useful with simple matches

    :param idx_match: the index matched
    :param simple: a flag indicating wether the index is simple or complex
    :returns: the normalized index
    """
    subidx = f"-{idx_match[3]}" if idx_match[3] is not None else ""
    idx = f"{idx_match[1]}_{idx_match[2]}{subidx}".lower()
    return idx


def decompose_index(idx) -> t.List[str]:
    """
    normalize indexes + decompose multi-level indexes (MC 1 à 4)
    into index arrays ([MC1, MC2, MC3, MC4])
    """
    def idx_simple_match(idx:str) -> str:
        """
        perform a simple match on a row's id:
        a simple match is an id composed of:
        `^{museum_code}{separator}?{hexadecimal_id}{separator}?{subid}?{separator}?$`
        1) museum_code: `MC`, `BNF` (compulsory)
        2) hexadecimal_id: `112`, `A9` (compulsory)
        3) subidentifier: `1`, `19` (ex: the last "1" in MC_112-1) (optionnal)
        4) separator: `-`, `\s`, `_` (optionnal): separators between
        =/= parts of `idx`.

        the regex used is kind of ugly to allow better group capture.

        :matched:
        MC112
        MC_112
        MC 112-
        MC_112-1
        :not_matched:
        MC
        mc112
        mc@112
        :subgroups: based on the example "MC 112-1"
        1) museum code:    "MC"
        2) hexadecimal id  "112"
        3) subidentifier   "1"

        :param idx: the identifier on which we'll be performing a match
        :returns: * if there's a match, a normalized index
                * if there's no match, an empty string
        """
        idx_norm = ""
        if not (pd.isna(idx)):
            simple_match = re.match(r"^([A-Z]+)[\s_-]*([A-Z\d]+)[\s_-]*(\d)*[\s_-]*$", idx)
            idx_norm = idx_normalise(simple_match) if simple_match else ""
        return idx_norm


    def idx_complex_match(idx: str) -> t.List[str]:
        """
        perform a complex match. this one is more complicated than
        a simple match (and it is not, in fact, a match):
        * we match `{museum_id}{separator}?{hexadecimal_id}` (`BNF 258`)
        to be sure that we're working with a valid row id.
        * we extract all other digits after the above pattern,
        which are interpreted as subindexes.
        * from that, we see how many images are contained within
        this complex id (2 for `BNF 258-1 et 2`, 4 for `BNF 258-1 à 4`
        and we decompose the complex array into an array of
        normalized image ids (`["BNF_258-1, "BNF_258-2", "BNF_258-3"]`)

        :param idx: the index to process
        :returns: * if we have a complex match: an array of rebuilt indexes.
                * else: an empty array
        """
        idx_norm = []
        base_match = re.search(r"^([A-Z]+)[\s_-]*([A-Z]*\d+)", idx)
        if base_match:
            _idx = idx
            idx = idx.replace(base_match[0], "")                      # only keep the non-matched part of `idx`
            subindexes = [ int(i) for i in re.findall(r"\d+", idx) ]  # multiple numberic sub-indexes
            if len(subindexes) > 0:
                # append all subindexes
                for s in range( min(subindexes), max(subindexes)+1 ):
                    idx_norm.append( f"{base_match[1]}_{base_match[2]}-{s}" )
        return idx_norm

    idx = idx.strip()     # type: ignore ;
    idx_simple = idx_simple_match(idx)     # normalized notation if simple, `""` if not simple
    idx_complex = idx_complex_match(idx)   # array of normalized index if complex, `[]` if not complex

    if not idx_complex:
        return [ idx_simple ]
    else:
        return idx_complex


def idx2filename(idxarr:t.List[str]) -> t.List[str]:
    """
    match an image file based on the csv row's index `idx`
    :param idx: the index we're working on
    :returns: the list of matched filenames
    """
    filenames = []
    images_in = [ os.path.join("in", "images", f)                    # full path for all images
                  for f in os.listdir(os.path.join("in", "images"))
                  if os.path.isfile(os.path.join("in", "images", f)) ]

    for idx in idxarr:
        for i in images_in:
            fname = os.path.basename(i)  # filename without path
            id_img = re.sub(r"\..*?$", "", os.path.basename(i)).lower()  # filename without path and extension

            if idx == id_img.lower() \
            or re.search(f"^{idx}[^a-zA-Z0-9]+$", id_img.lower()) \
            or re.sub(r"[_\s-]", "", idx.lower()) == re.sub(r"[_\s-]", "", id_img.lower()):
                    # perfect match between filename and index
                    # or near perfect match (i.e., the filename contains
                    # irrelevent characters at the end, or the filename
                    # matches `idx` without taking separators intro account)
                    filenames.append(fname)
    return filenames


# *********************************************************
# pipeline
# *********************************************************
def pipeline():
    # cli logic
    parser = argparse.ArgumentParser()
    parser.add_argument( "-n"
                       , "--numberofrows"
                       , help="size of the output (in number of rows)"
                       , type=int)
    parser.add_argument( "-d"
                       , "--directrepr"
                       , help="keep only direct representations of the neighbourhood"
                       , action="store_true" )
    args = parser.parse_args()
    if not args.numberofrows:
        print(f"* using default output size: 100 rows (no `--numberofrows` given)")
        sample_size = 100
    else:
        sample_size = args.numberofrows
        print(f"* output size: {sample_size} rows")
    if args.directrepr:
        direct = True
        print("* keeping only direct representations of the neighbourhood")
    else:
        direct = False
        print("* keeping both direct and indirect representations of the neighbourhood")


    # delete and create outputs
    curdir = os.path.abspath(os.path.dirname(__file__))
    d1 = os.path.join(curdir, "out")
    d2 = os.path.join(curdir, "out", "images")
    if os.path.isdir(d1):
        if os.path.isdir(d2):
            [ os.remove(os.path.join(d2, f)) for f in os.listdir(d2) ]  # empty out/images
            os.rmdir(d2)
        [ os.remove(os.path.join(d1, f)) for f in os.listdir(d1) ]      # empty out/
    os.makedirs(d2, exist_ok=True)

    # read the df
    df = pd.read_csv( os.path.join("in", "iconography_viviennebourse_240304.csv")
                    , sep="\t", names=ICONO_HEADERS, index_col=1
                    , header=0)
    df = df.loc[ df.index.notna() ]                  # remove nan indexes

    # filter the df based on `direct` + assert that `-n` < df.shape[0]
    if df.shape[0] < sample_size:
        raise ValueError(f"`--numberofrows` must be lower than {df.shape[0]} !")
    if direct:
        df = df.loc[ df.represents_richelieu.eq("OUI") ]
        if df.shape[0] < sample_size:
            raise ValueError(f"with `--directrepr`, `--numberofrows` must be lower than {df.shape[0]} !")

    # run the process
    df["idxarr"] = df.index.map(decompose_index)     # decompose the index if it is complex (points to >1 img)
    df["filenames"] = df.idxarr.apply(idx2filename)  # match filenames
    df = (df.loc[ df.filenames.apply(len).gt(0), [ "institution", "title"
                                                , "author_1", "author_2"
                                                , "publisher", "date_source"
                                                , "inventory_number", "filenames" ] ]
            .sample(n=sample_size, axis=0) )
    df.filenames.explode().apply(lambda x: shutil.copy2( os.path.join("in", "images", x)  # move the images to out
                                                       , os.path.join("out", "images", x)))
    df.to_csv(os.path.join("out", f"iconography_viviennebourse_240304_{sample_size}.csv")
             , sep="\t", index=False)

    # zip the output
    shutil.make_archive( f"randomcorpus_out_{sample_size}"
                       , "zip"
                       , os.path.join(curdir, "out") )

    return


if __name__ == "__main__":
    pipeline()

